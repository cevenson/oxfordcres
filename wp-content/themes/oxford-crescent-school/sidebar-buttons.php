<?php
 
// check if the repeater field has rows of data
if( have_rows('quick_link_buttons' , 'option') ):
 
 	// loop through the rows of data
    while ( have_rows('quick_link_buttons' , 'option') ) : the_row();?>
	<a class="quick-link-buttons" href="<?php the_sub_field('button_link' , 'option');?>" title="<?php the_sub_field('button_name' , 'option');?>">
       <?php the_sub_field('button_name' , 'option');?>
	</a>
 
   <?php endwhile;?>
 

 
<?php endif; ?>