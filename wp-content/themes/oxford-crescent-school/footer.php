<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>		
		</div> <!-- container -->
		</section><!-- #main -->
		<footer class="footer" role="contentinfo">
		<div class="container">

			<?php if(get_field('footer_phone', 'option')):?>

				<p class="footer-phone"><?php the_field('footer_phone' , 'option');?></p>
	
			<?php endif; ?>
			
			<?php if(get_field('footer_email_address', 'option')):?>
		
				<a href="mailto:<?php the_field('footer_email_address', 'option');?>" class="footer-email"><?php the_field('footer_email_address', 'option');?></a>
		
			<?php endif; ?>
			
			<?php if(get_field('footer_address', 'option')):?>

				<p class="footer-address"><?php the_field('footer_address', 'option');?></p>
	
			<?php endif; ?>
		</div>
		</footer><!-- footer -->
		
<!-- Load libraries and plugins -->
<!--
<script src='<?php bloginfo('template_directory'); ?>/js/vendor/jquery/jquery.scrollTo.min.js'></script>
<script src='<?php bloginfo('template_directory'); ?>/js/vendor/bootstrap-min.js'></script>

<script src='<?php bloginfo('template_directory'); ?>/js/navigation-manager.js'></script>
-->

<!--  Load flexslider -->
<!-- <script src='<?php bloginfo('template_directory'); ?>/js/vendor/jquery.flexslider-min.js'></script> -->

<!-- Remove any navigation patterns below that arent used -->
<!-- // @javascript nav-toggle -->
<!-- <script src='<?php bloginfo('template_directory'); ?>/js/nav-patterns/left-nav-flyout.js'></script> -->
<script src='<?php bloginfo('template_directory'); ?>/js/nav-patterns/responsive-nav.min.js'></script>

<!-- Load configuration and initialise JavaScripts. 
	 Look in config.js to see how to turn items on/off -->
<script src='<?php bloginfo('template_directory'); ?>/js/config.js'></script>
<script src='<?php bloginfo('template_directory'); ?>/js/main.js'></script>
		
		
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
	</body>
</html>